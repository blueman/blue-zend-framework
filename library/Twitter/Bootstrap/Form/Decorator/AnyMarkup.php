<?php

class Twitter_Bootstrap_Form_Decorator_AnyMarkup extends Zend_Form_Decorator_Abstract {
 
    public function render($content) {
        $placement = '';
        $content = trim($content);

        if (isset($this->_options['inputPrepend'])){
            $placement .= 'input-prepend ';
            $content = $this->_options['inputPrepend'] . $content;
        }
        if (isset($this->_options['inputAppend'])){
           $placement .= ' input-append ';
            $content .= $this->_options['inputAppend'];
        }

        if (isset($this->_options['prepend'])){
            $content = $this->_options['prepend'] . $content;
        }
        if (isset($this->_options['append'])){
            $content .= $this->_options['append'];
        }

        return '<div class="' . $placement . '">' . $content . '</div>';

        return ($content);
    }
 
}