<?php

/**
 * Db profiler to log long queries.
 *
 * You can extend the class by Zend_Db_Profiler or as well by Zend_Db_Profiler_Firebug - the main (and only one?)
 * different is that Profiler_Firebug will also output queries in Firebug console.
 *
 * In production machine Firebug shouldnt be switch on!
 *
 * @author      Szymon Bluma [BlueMan] <szbluma@gmail.com>
 * @link        http://www.blueman.pl/
 * @copyright   CC BY <http://creativecommons.org/licenses/by/3.0/>
 *
 */
class MrBlue_Db_Profiler extends Zend_Db_Profiler_Firebug
{
    /**
     * Zend_Log instance
     * @var Zend_Log
     */
    protected $_logQuery;
    
    /**
     * setting query time from which we will log the "long query"
     * @var float
     */
    protected $_queryMaxTime = 1.0;    // in seconds, set to 0 (zero) to disable logging
    
    /**
     * Constructor
     * @param string|bool $label
     */
    public function __construct($label = 'Project Queries') {
        
        if( get_parent_class()=='Zend_Db_Profiler' && !is_bool($label) ) {
            $label = false;
        }
        
        parent::__construct($label);
        
        $this->_logQuery = new Zend_Log();
        $writer = new Zend_Log_Writer_Stream(SOURCE_PATH . '/data/logs/db_long_queries.log');
        $this->_logQuery->addWriter($writer);
    }
    
    /**
     * Intercept the query end and log the profiling data.
     *
     * @param  integer $queryId
     * @throws Zend_Db_Profiler_Exception
     * @return void
     */
    public function queryEnd($queryId) {
        $state = parent::queryEnd($queryId);
        
        if (!$this->getEnabled() || $state == self::IGNORED) {
            return;
        }
        
        // get profile of the current query
        $profile = $this->getQueryProfile($queryId);
        
        // query time
        $queryTime = $profile->getElapsedSecs();
        
        if( $this->_queryMaxTime>0 && $queryTime>$this->_queryMaxTime ) {
            // create the message to be logged
            $message = "\r\nElapsed Secs: " . round($queryTime, 5) . "\r\n";
            $message .= "Query: " . $profile->getQuery() . "\r\n";
            
            // log the message as INFO message
            $this->_logQuery->info($message);
        }
        
    }
}