<?php
/**
 * Backend form
 *
 * @author    Szymon Bluma
 * @copyright Szymon [BlueMan] Bluma
 * @link      http://www.blueman.pl
 */
class MrBlue_Form_Backend extends Twitter_Bootstrap_Form_Vertical
{
    protected $_controller;

    protected $_action;

    protected $_params;
    
    /**
     * Auth
     *
     * @var Zend_Auth
     * @access protected
     */
    protected $_auth = null;

    protected $elementFileDecorators = array(
        'File',
        'Errors',
        array('Label', array()),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'line')),
    );

    protected $elementSearchDecorators = array(
        'ViewHelper',
        'Errors',
        array('Label', array()),
    );

    protected $elementCaptchaDecorators = array(
        'Errors',
        array('Label', array()),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'line')),
    );

    protected $onlyDescriptionElementDecorators = array(
        array('Description', array('tag' => '')),
        array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'line')),
    );

    public function __construct()
    {
        $this->_controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $this->_action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
        $this->_params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
        
        if (Zend_Registry::isRegistered('auth')) {
            $this->_auth = Zend_Registry::get('auth');
        }

        $this->setName('form');
        $this->setMethod('post');
        $this->setAction($_SERVER['REQUEST_URI']);
        $this->_addClassNames('well');

        parent::__construct();
        
        // our custm validators
        $this->addElementPrefixPath(
                'MrBlue_Validate',
                SOURCE_PATH.'/library/MrBlue/Validate/',
                'validate'
        );

        $this->setRequired();
    }

    protected function setRequired()
    {
        foreach ($this->getElements() as $element) {
            $label = $element->getDecorator('label');
            if ($label) {
                $label->setOption('requiredSuffix', ' *');
            }
        }
    }

    protected function disableElements($elements = array())
    {
        if (empty($elements)) {
            $elements = $this->getElements();
        }

        foreach ($elements as $element) {
            if (is_string($element)) {
                $element = $this->getElement($element);
            }

            $label = $element->getDecorator('label');
            if ($label && $element != 'login') {
                $element->setAttrib('disabled', 'disabled');
            }
        }
    }

    protected function addSubmitButton($title = 'Save', $class = null, $goBack = array())
    {
        $_icon = '';

        switch ($title) {
            case 'Save':
                $_icon = '<span class="icon-ok icon-white"></span> ';
                break;

            case 'Create':
                $_icon = '<span class="icon-plus icon-white"></span> ';
                break;
        }

        $_class = ' class="btn btn-primary" ';

        if (!empty($class)) {
            $_class = ' class="' . $class . '" ';
        }

        $goBackButton = '';

        if(!empty($goBack)) {
            $goBackButton = '<span style="padding-left: 5px;">or <a style="padding-left: 0;" class="btn btn-link" '
                . 'title="' . ucfirst($goBack['title']) . '" href="' . $goBack['url'] . '">' . $goBack['title'] . '</a></span>';
        }

        $this->addElement('text', 'submitButton', array(
            'description' => '<button' . $_class . 'type="submit">' . $_icon . $title . '</button>' . $goBackButton,
            'required' => false,
            'decorators' => $this->onlyDescriptionElementDecorators,
        ));

        $this->getElement('submitButton')->getDecorator('Description')->setOption('escape', false);
    }

    protected function addRequiredInformation()
    {
        $this->addElement('text', 'requiredInformation', array(
            'description' => '<div class="alert alert-block alert-info"><p>Fields marked with * (an asterix) are required.</p></div>',
            'required' => false,
            'decorators' => $this->onlyDescriptionElementDecorators,
        ));

        $this->getElement('requiredInformation')->getDecorator('Description')->setOption('escape', false);
    }

    /**
     * Get identity
     *
     * @return Zend_Auth User identity
     */
    protected function getIdentity()
    {
        return $this->_auth->getIdentity();
    }

    /**
     * Get Zend_Acl
     *
     * @return Zend_Acl
     */
    protected function getAcl()
    {
        return Zend_Registry::get('acl');
    }
}