<?php
/**
 * Benchmark helper
 *
 * @author      Szymon Bluma [BlueMan] <szbluma@gmail.com>
 * @link        http://www.blueman.pl/
 * @copyright   CC BY <http://creativecommons.org/licenses/by/3.0/>
 */
class MrBlue_Helper_Benchmark
{
    public function renderFooter()
    {
        $aQueryStats = $this->getQueryStats();
        return '<div class="benchmark pull-right">
                    <p>Page generated in <em class="text-info">' . $this->getRuntime()
            . ' sec.</em> using <em class="text-info">' . $this->getMemoryUsage() . ' MB</em> memory.</p>
                    <p>SQL: <em class="text-info">' . $aQueryStats['query_count']
            . ' queries</em> in <em class="text-info">' . $aQueryStats['total_time'] . ' sec.</em>,
                          longest in <em class="text-warning">' . $aQueryStats['longest_time'] . ' sec.</em></p>
                </div>';
    }
    
    /**
     * Returns the peak of memory, in megabytes, that's been allocated to PHP script.
     *  
     * @return float megabytes
     */
    public function getMemoryUsage()
    {
        return number_format(memory_get_peak_usage()/1024/1024, 2);
    }
    
    /**
     * Get rendering time of application
     * 
     * @return float seconds
     */
    public function getRuntime()
    {
        return number_format(array_sum(explode(' ',microtime())) - ATS, 4);
    }
    
    
    /**
     * Get query profiler stats
     * 
     * @return array
     */
    public function getQueryStats()
    {
        // (default) array to return by this function
        $aStats = array(
            'total_time' => 0,
            'query_count' => 0,
            'longest_time' => 0,
        );
        
        // db profiller is enabled only for development enviroment
        if( Zend_Registry::isRegistered('dbAdapter') && APPLICATION_ENV == 'development' ) {
            $db = Zend_Registry::get('dbAdapter');
            $profiler = $db->getProfiler();
            $aStats['total_time'] = number_format($profiler->getTotalElapsedSecs(), 4);
            $aStats['query_count'] = $profiler->getTotalNumQueries();
            
            if( is_array($profiler->getQueryProfiles()) ) {
                foreach ($profiler->getQueryProfiles() as $query) {
                    if ( $query->getElapsedSecs()>$aStats['longest_time'] ) {
                        $aStats['longest_time']  = number_format($query->getElapsedSecs(), 4);
                    }
                }
            }
        }
        
        return $aStats;
    }
    
}
