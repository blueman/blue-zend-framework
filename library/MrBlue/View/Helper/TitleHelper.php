<?php
/**
 * Page title helper
 *
 * @author      Szymon Bluma [BlueMan] <szbluma@gmail.com>
 * @link        http://www.blueman.pl/
 * @copyright   CC BY <http://creativecommons.org/licenses/by/3.0/>
 */
class MrBlue_View_Helper_TitleHelper extends Zend_View_Helper_Abstract
{
    public function TitleHelper($last = true)
    {
        if($last) {
            $headTitle = explode($this->view->headTitle()->getSeparator(), strip_tags($this->view->headTitle()));
            return end($headTitle);
        }
        else {
            return strip_tags($this->view->headTitle());
        }
    }
}