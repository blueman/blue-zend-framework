<?php
/**
 * SuperAdmin.php
 *
 * @author      Szymon Bluma [BlueMan] <szbluma@gmail.com>
 * @link        http://www.blueman.pl/
 * @copyright   CC BY <http://creativecommons.org/licenses/by/3.0/>
 */
abstract class MrBlue_Controller_Action_SuperAdmin extends Zend_Controller_Action
{
    /**
     * FlashMessenger
     *
     * @var object
     * @access protected
     */
    protected $_flashMessenger = null;


    /**
     * Array with params (controller, action, etc)
     *
     * @var array
     * @access protected
     */
    protected $params;


    /**
     * Application config object
     *
     * @var Zend_Config
     * @access protected
     */
    protected $config;


    /**
     * MrBlue Benchamark Helper
     *
     * @var MrBlue_Helper_Benchmark
     * @access protected
     */
    protected $benchmark;


    /**
     * Zend_Auth
     *
     * @var Zend_Auth
     * @access protected
     */
    protected $_auth = null;


    /**
     * User account
     *
     * @var object
     * @access protected
     */
    protected $account = null;


    /**
     * Array of post values
     * @var array
     */
    protected $aPost = array();


    /**
     *  init()
     *
     * @access public
     */
    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->messages = $this->_flashMessenger->getMessages();

        $this->params = $this->getAllParams();
        $this->view->params = $this->params;

        if ( $this->_request->isPost() ) {
            $this->aPost = $this->_request->getPost();
        }
        $this->view->aPost = $this->aPost;

        $this->config = Zend_Registry::get('config');
        $this->view->config = $this->config;

        $this->benchmark = new MrBlue_Helper_Benchmark();
        $this->view->benchmark = $this->benchmark;

        $this->setAuth();
    }

    /**
     * Set auth
     *
     * @access public
     * @return void
     */
    public function setAuth() {
        $this->_auth = Zend_Auth::getInstance();
        $this->_auth->setStorage(new Zend_Auth_Storage_Session('Admin_Auth'));

        if ($this->_auth->hasIdentity()) {
            $this->setAccountData();
        }
    }

    /**
     * Set account data
     *
     * @access public
     * @return void
     */
    public function setAccountData()
    {
        if($this->_auth->hasIdentity()) {
            //$signedInNav = require APPLICATION_PATH . '/modules/default/configs/navigation_signedin.php';

            $this->account = $this->_auth->getIdentity();
            Zend_Registry::set('account', $this->account);

            $this->view->account = $this->account;
            //$this->view->mainNav->removePage($this->view->mainNav->findOneByLabel('Sign Up'));
            //$this->view->mainNav->removePage($this->view->mainNav->findOneByLabel('Sign In'));
            //$this->view->mainNav->addPages($signedInNav);
            //$this->view->mainNav->findOneByLabel('Profile')->setLabel($this->_auth->getIdentity()->email);
        }
    }
}
