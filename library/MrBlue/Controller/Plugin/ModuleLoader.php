<?php

class MrBlue_Controller_Plugin_ModuleLoader extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $module = $request->getModuleName();

        $path = APPLICATION_PATH . '/modules/' . $module . '/Bootstrap.php';

        $class = ucfirst($module) . '_Bootstrap';

        $application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/modules/' . $module . '/configs/module.ini'
        );

        Zend_Loader::loadFile('Bootstrap.php', dirname($path));
        $bootstrap = new $class($application);
        $bootstrap->bootstrap();
    }
}