<?php

/**
 * Fetching log error function
 *
 * @param int $intErrNo				level of the error raised
 * @param string $strErrStr			error message
 * @param string $strErrFile		filename that the error was raised in
 * @param string $strErrLine		line number the error was raised at
 * @param string $strErrContext		variable that existed in the scope the error was triggered in
 *
 */
function newErrorHandler( $intErrNo, $strErrStr, $strErrFile, $strErrLine, $strErrContext )
{
	if (error_reporting() & $intErrNo) {
		/*
		 * Starting from ZF 1.10.1 version, method isReadable() in Zend/Loader.php throw an error
		 * while useing the is_readable() function (line ~198).
		 * So I made some "dirty hack" in this place :)
		 */
		if ($intErrNo===E_WARNING && strpos($strErrFile, 'Zend/Loader.php') !== false && strpos($strErrStr, 'open_basedir restriction in effect') !== false) {
			return;
		}
		
		/*
		 * Becouse of usage EliteROI_Db_* and create new Select query function some of the parent Zend_Db_* class
		 * throw an error of incompatible type given.
		 */
		if( $intErrNo===E_RECOVERABLE_ERROR && strpos($strErrFile, 'Zend/Db/') !== false && strpos($strErrStr, 'must be an instance of Zend_Db_Table_Select, instance of EliteROI_Db_') !== false) {
		    return ;
		}

		genErrorMsg( $intErrNo, $strErrStr, $strErrFile, $strErrLine, $strErrContext );
	}
}


/**
 * Function making an error message
 *
 * @param int $strErrNo			OPTIONAL error number
 * @param string $strError		OPTIONAL error description
 * @param string $strFile		OPTIONAL filename of error was raised in
 * @param string $strLine		OPTIONAL line number where error was raised in
 * @param string $mDetails		OPTIONAL extended information of the error
 * @param string $sNewFile		OPTIONAL filename where save the error information
 *
 * @return array
 *
 */
function genErrorMsg( $strErrNo=0, $strError=NULL, $strFile=NULL, $strLine=NULL, $mDetails=NULL, $sNewFile=NULL )
{
	$arrError['errorNo'] = 0;
	
	$serverHttpHost = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'domain-cli.com';
	$serverRequestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : 'domain-cli.com/url';

	$strErrMsg    = "-\n";
	$strErrMsg   .= "DATETIME : " . date( "Y-m-d H:i:s" ) . "\n";
	$strErrMsg   .= "DOMAIN   : ". $serverHttpHost ."\n";
	if ( !is_null($strFile) ) {
		$strErrMsg   .= "FILE     : " . $strFile . "\n";
	}
	$strErrMsg   .= "URL      : " . $serverRequestUri . "\n";
	if ( !is_null($strLine) ) {
		$strErrMsg   .= "LINE     : " . $strLine . "\n";
	}
	$strErrMsg	 .= "TYPE	 : " . errorNoToString( $strErrNo ) . "\n";
	$strErrMsg   .= "ERROR    : " . $strError . "\n";

	if( !empty( $mDetails ) && is_string( $mDetails ) ) {
		$strErrMsg   .= "DETAILS  : " . $mDetails . "\n";
	}
	
	$sFileName = 'php_log.txt';
	if ( !is_null($sNewFile) ) {
		$sFileName = $sNewFile;
	}
	elseif( !Zend_Registry::get('config')->phplogs->common_log_file && is_null($sNewFile) ) {
		$sFileName = 'php_unknow_error_log.txt';
		$aKonfiguracja = array_flip( Zend_Registry::get('config')->phplogs->errors_files->toArray() );
		foreach( $aKonfiguracja as $iSumaBitowa => $sPlik ){
			if( $iSumaBitowa & (int)$strErrNo ){
				$sFileName = $sPlik;
				break;
			}
		}
		unset($aKonfiguracja);
	}

	error_log( $strErrMsg, 3, PHP_ERROR_LOG_DIR.DS.$sFileName );

	$arrError = array();
	if( !empty( $strError ) ) {
		$arrError['errorMsg'] = $strError;
	}

	unset($strErrMsg);

	return $arrError;
}


/**
 * Fast method to save some custom error/message in log dir
 *
 * @param int $strErrNo		OPTIONAL error identificator
 * @param string $strError	OPTIONAL error message
 * @param string $sNewFile	OPTIONAL in which file save the error?
 * @return array
 */
function saveErrorMsg($strErrNo=0, $strError=NULL, $sNewFile=NULL)
{
	return genErrorMsg($strErrNo, $strError, $strFile=NULL, $strLine=NULL, $mDetails=NULL, $sNewFile);
}


/**
 * Saving error helper for invalid instanceof errors
 *
 * @param string $sClassName	Class name which we are expected, but the $mVariable wasnt
 * @param mixed $mVariable		Passed variable
 * @param string $sFileName		Where the error raised
 * @param int $iLineNumber		Where the error raised
 */
function saveInvalidInstanceError($sClassName, $mVariable, $sFileName, $iLineNumber)
{
	genErrorMsg(E_WARNING, 'Argument wasnt a '.$sClassName.', '.gettype($mVariable).' given', $sFileName, $iLineNumber, NULL, "php_instanceof_errors.txt");
}


/**
 * Error number converting into human readable format :)
 *
 * @param int $iErrNo	error number
 * @return string
 */
function errorNoToString( $iErrNo )
{
	switch( $iErrNo ){
		case E_ERROR:
			return "Error";
			break;
		case E_WARNING:
			return "Warning";
			break;
		case E_PARSE:
			return "Parse Error";
			break;
		case E_NOTICE:
			return "Notice";
			break;
		case E_CORE_ERROR:
			return "Core Error";
			break;
		case E_CORE_WARNING:
			return "Core Warning";
			break;
		case E_COMPILE_ERROR:
			return "Compile Error";
			break;
		case E_COMPILE_WARNING:
			return "Compile Warning";
			break;
		case E_USER_ERROR:
			return "User Error";
			break;
		case E_USER_WARNING:
			return "User Warning";
			break;
		case E_USER_NOTICE:
			return "User Notice";
			break;
		case E_STRICT:
			return "Strict Notice";
			break;
		case E_RECOVERABLE_ERROR:
			return "Recoverable Error";
			break;
		default:
			return "Unknown error ($iErrNo)";
			break;
	}
}


/**
 * saving exception information
 *
 * @param Exception $e		exception object
 * @return false|array
 */
function saveException( $e )
{
	if( !($e instanceof Exception) ) {
		return false;
	}

	$trace = $e->getTrace();
	$aTrace = current($trace);

	return genErrorMsg( $e->getCode(), 'Function: '.$aTrace['function'].' said '.$e->getMessage(), $e->getFile(), $e->getLine());
}

function initDirStructure()
{
	// the source path must be writable to apply the chmods and mkdirs

	$dirs = array(
		'data' => 0777,
		'data/logs' => 0777,
		'data/sessions' => 0777,
		'data/cache' => 0777,
	);

	foreach( $dirs as $dir=>$chmod ) {
		if( !is_dir(SOURCE_PATH.DS.$dir) ) {
			@mkdir(SOURCE_PATH.DS.$dir, $chmod, true);
			@chmod(SOURCE_PATH.DS.$dir, $chmod);
		}
	}
}


/**
 * Rounding up float number with specify precision
 *
 * @param float $number     Number to round
 * @param int $prec         Expected precision
 * @return float
 */
function roundup_prec($in,$prec)
{
    $fact = pow(10,$prec);
    return ceil($fact*$in)/$fact;
}
            