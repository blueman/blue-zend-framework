<?php

class Project {
	
	/**
	 * Initialize private log dir for php error, warning, info and other errors
	 */
	public static function initLogDir()
	{	
		if( !is_dir(SOURCE_PATH.'/data/logs') ) {
			@mkdir(SOURCE_PATH.'/data/logs', 0777, true);
			@chmod(SOURCE_PATH.'/data/logs', 0777);
		}
		
		define('PHP_ERROR_LOG_DIR', SOURCE_PATH.'/data/logs' );
		set_error_handler("newErrorHandler");	// newErrorHandler() is in functions.php
		
		$logger = new Zend_Log();
		$logger->setEventItem('userip', isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '-cmd-');
		$logger->setEventItem('timestamp', date('Y-m-d H:i:s'));
		$format = "DATETIME : %timestamp%".PHP_EOL."USERIP	 : %userip%".PHP_EOL."PRIORITY : %priorityName% (%priority%)".PHP_EOL."ERROR    : %message%".PHP_EOL."-".PHP_EOL;
		$writer = new Zend_Log_Writer_Stream(PHP_ERROR_LOG_DIR.'/zend_logger.log');
		$formatter = new Zend_Log_Formatter_Simple($format);
		$writer->setFormatter($formatter);
		$logger->addWriter($writer);
		Zend_Registry::set('logger', $logger);
		
	}
	
}