<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initModules() {

    }

    protected function _initDbAdapter() {
        $this->bootstrap('db');
        $db = $this->getPluginResource('db');
        Zend_Registry::set('db', $db->getDbAdapter());
    }

    protected function _initPlugins() {
        $this->bootstrap('frontController');
        Zend_Layout::startMvc();
    }

    protected function _initConfig()
    {
        // email init
        $mailFile = APPLICATION_PATH . '/configs/mail.ini';
        if (!file_exists($mailFile)) {
            $mailFile .= '.dist';
        }
        $mail = new Zend_Config_Ini($mailFile, APPLICATION_ENV);

        // default config init
        $config = new Zend_Config($this->getOptions(), true);
        $config->merge($mail);
        $config->setReadOnly();
        Zend_Registry::set('config', $config);

        Project::initLogDir();
    }

    protected function _initCache() {
        $frontendOptions = array(
            'lifetime'                  => 18000,
            'automatic_serialization'   => true,
            'automatic_cleaning_factor' => 5,
        );

        $backendOptions = array(
            'cache_dir' => SOURCE_PATH . '/data/cache',
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

        Zend_Registry::set('cache', $cache);
    }

    protected function _initDbCache() {
        $cache = Zend_Registry::get('cache');
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }

    public function _initRoutes() {
        $frontController = Zend_Controller_Front::getInstance();

        $rConfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes.ini');
        $router = $frontController->getRouter();
        $router->addConfig($rConfig, 'routes');
        $frontController->setRouter($router);
    }

}
