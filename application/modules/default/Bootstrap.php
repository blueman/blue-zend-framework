<?php

class Default_Bootstrap extends Zend_Application_Module_Bootstrap {

    /**
     * Presentation layer
     *
     * @return void
     */
    protected function _initPresentation()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        # doctype
        $view->doctype('HTML5');


        # headTitle
        $view->headTitle('SportLink')
            ->setSeparator(' / ');
    }

}
