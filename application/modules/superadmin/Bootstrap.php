<?php

class SuperAdmin_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     *  Presentation layer
     *
     * @return void
     */
    protected function _initPresentation()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        // doctype
        $view->doctype('HTML5');

        // view helpers
        $view->addHelperPath('MrBlue/View/Helper/', 'MrBlue_View_Helper');

        // stylesheets
        $view->headLink()->appendStylesheet('/css/bootstrap.min.css');
        $view->headLink()->appendStylesheet('/css/bootstrap-responsive.min.css');
        $view->headLink()->appendStylesheet('/css/jui-custom/jquery-ui-1.10.3.custom.min.css');

        // scripts
        $view->headScript()->appendFile('/js-libraries/html5shiv.js', 'text/javascript', array('conditional' => 'lt IE 9'));
        $view->headScript()->appendFile('/js-libraries/jquery-2.0.3.min.js', 'text/javascript');
        $view->headScript()->appendFile('/js-libraries/jquery-ui-1.10.3.custom.min.js', 'text/javascript');
        $view->headScript()->appendFile('/js-libraries/bootstrap.min.js', 'text/javascript');
        $view->headScript()->appendFile('/js/superadmin/_site.js', 'text/javascript');
        
        if( file_exists(PUBLIC_PATH.DS.'js'.DS.'superadmin'.DS.'index.js') ){
            $view->headScript()->appendFile('/js/superadmin/index.js', 'text/javascript');
        }
        
        // head title
        $view->headTitle('SportLink SuperAdmin')
            ->setSeparator(' / ');
    }
}
