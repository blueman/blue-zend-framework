<?php
/**
 * Sign in form
 *
 * @author      Szymon Bluma [BlueMan] <szbluma@gmail.com>
 * @link        http://www.blueman.pl/
 * @copyright   CC BY <http://creativecommons.org/licenses/by/3.0/>
 */
class SuperAdmin_Form_SignIn extends MrBlue_Form_Backend
{
    /**
     * init()
     *
     */
    public function init()
    {
        $this->_addClassNames('form-signin');

        $this->addElement('text', 'test', array(
            'description' => '<h2 class="form-signin-heading">Please sign in</h2>',
            'ignore' => true,
            'decorators' => array(
                array('Description', array('escape' => false, 'tag' => '')),
            ),
        ));

        $this->addElement('text', 'email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', false, array(3, 64)),
                'EmailAddress'
            ),
            'required' => true,
            'placeholder' => 'E-mail address',
            'class' => 'input-block-level',
        ));

        $this->addElement('password', 'password', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('StringLength', false, array(4, 100)),
            ),
            'required' => true,
            'placeholder' => 'Password',
            'class' => 'input-block-level',
        ));

        $this->addSubmitButton('Sign In', 'btn btn-large btn-primary');
    }
}